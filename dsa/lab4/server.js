const net = require('net');

const server = net.createServer(function(socket) {
  const messages = []
  const isTimerStarted = false

  const startTimer = () => {
    setTimeout(() => {
      sendMessages()
    }, 10000)
  }

  const sendMessages = () => {
    messages.forEach(message => {
      socket.write(message)
    })
  }

  socket.on("error", (err) =>
    console.log(`Caught flash policy server socket error: ${err}`)
  )
	socket.write('Echo server\r\n');
	socket.pipe(socket);

  socket.on('data', (data) => {
    messages.push(data.toString('utf-8'))

    if(!isTimerStarted) {
      startTimer()
    }
  })
});

server.listen(1502, '127.0.0.1');