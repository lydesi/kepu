const { Worker } = require("worker_threads");

let canRun = true;

const THREADS_AMOUNT = 4;

(function () {
  // Array of promises
  const promises = [];
  for (let idx = 0; idx < THREADS_AMOUNT; idx += 1) {
    promises.push(new Promise((resolve) => {
      const worker = new Worker("./thread-entry.js", {
        workerData: {
          workerId: idx
        }
      });
      worker.on("exit", () => {
        console.log("Closing ", idx);
        resolve()
      });
      worker.on("message", (value) => {
        console.log(`message received from ${idx}: ${value}`);
      })
    }))
  }
  return Promise.all(promises);
})().then(() => {
  canRun = false;
})

function infiniteLoop() {
  setTimeout(() => {
    if (canRun) {
      infiniteLoop();
    }
  }, 1000)
}

infiniteLoop();