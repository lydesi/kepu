const { parentPort, workerData } = require("worker_threads");
parentPort.postMessage(`ThreadId: ${workerData.workerId}`)

class ThreadInstance {
  savagesCount = 5
  portionsCount = 0
  portionEated = {}
  orderInProgress = false
  minPortionEated = 0

  order = () => {
    parentPort.postMessage(`ThreadId: ${workerData.workerId}; Order in progress`)
  
    if(!this.orderInProgress) {
      this.orderInProgress = true
      this.portionsCount = 10
      this.orderInProgress = false
      parentPort.postMessage(`ThreadId: ${workerData.workerId}; Order done`)
    } else {
      parentPort.postMessage(`ThreadId: ${workerData.workerId}; Order already ready`)
    }
  }
  
  eat = (sid) => {
    if(!this.portionEated[sid]) {
      this.portionEated[sid] = 1
    }

    if(this.minPortionEated + 1 < this.portionEated[sid]) { return }
  
    parentPort.postMessage(`ThreadId: ${workerData.workerId}; Portion eated by ${sid}`)
    this.portionsCount--
  
    if (this.portionEated[sid] === this.minPortionEated) {
      this.portionEated[sid]++
      this.minPortionEated++
    }
  
    if(this.portionEated[sid] < this.minPortionEated) {
      this.minPortionEated = this.portionEated[sid]
    }
  
    parentPort.postMessage(`ThreadId: ${workerData.workerId}; Savage ${this.sid} eated ${this.portionEated[sid]} portions`)
  }
  
  run = () => {
    for(let i = 0; i < this.savagesCount; i++) {
      if(this.portionsCount <= 0) {
        if(!this.orderInProgress) { this.order() }
      } else {
        this.eat(i)
      }
    }
  }
}

new ThreadInstance().run()

parentPort.close();
