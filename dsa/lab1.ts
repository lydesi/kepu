class Students {
  filterByFaculty(students: Student[], faculty: string) {
    return students.filter(student => student.faculty === faculty)
  }

  filterByBirthDate (students: Student[], birthDate: Date) {
    return students.filter(student => student.birthDate < birthDate)
  }

  filterByGroup (students: Student[], group: string)  {
    return students.filter(student => student.group === group)
  }
}

class Student {
  _id: string;
  _lastName: string;
  _firstName: string;
  _patronymic: string;
  _birthDate: Date;
  _address: string;
  _phoneNumber: string;
  _faculty: string;
  _course: string;
  _group: string;

  constructor(
    id: string,
    lastName: string,
    firstName: string,
    patronymic: string,
    birthDate: Date,
    address: string,
    phoneNumber: string,
    faculty: string,
    course: string,
    group: string
  ) {
    this._id = id
    this._lastName = lastName
    this._firstName = firstName
    this._patronymic = patronymic
    this._birthDate = birthDate
    this._address = address
    this._phoneNumber = phoneNumber
    this._faculty = faculty
    this._course = course
    this._group = group
  }

  get id (): string {
    return this._id
  }

  set id (id: string) {
    this._id = id
  }

  get faculty(): string {
    return this._faculty
  }

  get birthDate(): Date {
    return this._birthDate
  }

  get group(): string {
    return this._group
  }
}

const students: Student[] = []

const st1 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91')
const st2 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'KEPU', '4', 'IO-z91')
const st3 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91')
const st4 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91')

students.push(st1)
students.push(st2)
students.push(st3)
students.push(st4)

const sudentsSort = new Students()

console.log(sudentsSort.filterByFaculty(students, 'KEPU'))

console.log(students)


