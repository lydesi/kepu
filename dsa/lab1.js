var Students = /** @class */ (function () {
    function Students() {
    }
    Students.prototype.filterByFaculty = function (students, faculty) {
        return students.filter(function (student) { return student.faculty === faculty; });
    };
    Students.prototype.filterByBirthDate = function (students, birthDate) {
        return students.filter(function (student) { return student.birthDate < birthDate; });
    };
    Students.prototype.filterByGroup = function (students, group) {
        return students.filter(function (student) { return student.group === group; });
    };
    return Students;
}());
// видишь вот эту штуку
// students: Student[]
// вот у нее после : есть шутка Student[]
// это типизация
// ее нет в дефолтном js
// да код js это просто скомпилированный из ts
var Student = /** @class */ (function () {
    function Student(id, lastName, firstName, patronymic, birthDate, address, phoneNumber, faculty, course, group) {
        this._id = id;
        this._lastName = lastName;
        this._firstName = firstName;
        this._patronymic = patronymic;
        this._birthDate = birthDate;
        this._address = address;
        this._phoneNumber = phoneNumber;
        this._faculty = faculty;
        this._course = course;
        this._group = group;
    }
    Object.defineProperty(Student.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (id) {
            this._id = id;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "faculty", {
        // короче ts просто писать проще и нагляднее
        // нет
        // тс это просто расширение
        get: function () {
            return this._faculty;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "birthDate", {
        get: function () {
            return this._birthDate;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Student.prototype, "group", {
        get: function () {
            return this._group;
        },
        enumerable: false,
        configurable: true
    });
    return Student;
}());
var students = [];
var st1 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91');
var st2 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'KEPU', '4', 'IO-z91');
var st3 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91');
var st4 = new Student('1', 'lastname', 'firstname', 'patronymic', new Date(), 'address', '000', 'FIOT', '4', 'IO-z91');
students.push(st1);
students.push(st2);
students.push(st3);
students.push(st4);
var sudentsSort = new Students();
console.log(sudentsSort.filterByFaculty(students, 'KEPU'));
// 1 da
// ну и вот так все сортировки можно юзнуть
console.log(students);
