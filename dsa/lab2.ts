class Autopark {
  totalCost (cars: Car[]) {
    return cars.reduce((sum, car) => sum + car.price, 0)
  }

  sortByFuelConsuption(cars: Car[]) {
    return cars.sort((a, b) => a.fuelConsuption - b.fuelConsuption)
  }

  searchBySpeed (cars: Car[]) {
    return cars.sort((a,b) => a.maxSpeed - b.maxSpeed)
  }
}

class Car {
  _manufacturer: string
  _type: string
  _fuelConsuption: number
  _maxSpeed: number
  _price: number

  constructor(
    manufacturer: string,
    type: string,
    fuelConsumption: number,
    maxSpeed: number,
    price: number
  ) {
    this._manufacturer = manufacturer
    this._type = type
    this._fuelConsuption = fuelConsumption
    this._maxSpeed = maxSpeed
    this._price = price
  }

  get manufacturer (): string {
    return this._manufacturer
  }

  set manufacturer (manufacturer: string) {
    this._manufacturer = manufacturer
  }

  get type (): string {
    return this._type
  }

  set type (type: string) {
    this._type = type
  }

  get fuelConsuption (): number {
    return this._fuelConsuption
  }

  set fuelConsuption (fuelConsuption: number) {
    this._fuelConsuption = fuelConsuption
  }

  get maxSpeed (): number {
    return this._maxSpeed
  }

  set maxSpeed (maxSpeed: number) {
    this._maxSpeed = maxSpeed
  }

  get price (): number {
    return this._price
  }

  set price (price: number) {
    this._price = price
  }

  public toString(): string {
    return `Car manufacturer is ${this.manufacturer}, type is ${this.type}, fuel consuption id ${this.fuelConsuption} and max speed is ${this.maxSpeed}`
  }
}

class ElectricCar extends Car {
  _batteryCapacity: number
  _power: number
  _numberOfMotors: number

  constructor(
    manufacturer: string,
    type: string,
    fuelConsumption: number,
    maxSpeed: number,
    price: number,

    batteryCapacity: number,
    power: number,
    numberOfMotors: number,
  ) {
    super(manufacturer, type, fuelConsumption, maxSpeed, price)

    this._batteryCapacity = batteryCapacity
    this._power = power
    this._numberOfMotors = numberOfMotors
  }
}

const cars: Car[] = [
  new Car('Volkswagen', 'sedan', 8.0, 180, 15000),
  new Car('BMW', 'suv', 12.0, 180, 30000),
  new Car('Mercedes', 'sedan', 9.0, 200, 30000),
  new ElectricCar('Tesla', 'sedan', 9.0, 200, 30000, 1000, 500, 2),
]

const autopark = new Autopark()

const cost = autopark.totalCost(cars)
const fastestCars = autopark.searchBySpeed(cars)
const sortedByFuelConsuption = autopark.sortByFuelConsuption(cars)

console.log(`Cost is ${cost}, Cars sorted by fastest is ${fastestCars} and sorted by fuel consuption ${sortedByFuelConsuption}`)
