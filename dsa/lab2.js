var Autopark = /** @class */ (function () {
    function Autopark() {
    }
    Autopark.prototype.totalCost = function (cars) {
        return cars.reduce(function (sum, car) { return sum + car.price; }, 0);
    };
    Autopark.prototype.sortByFuelConsuption = function (cars) {
        return cars.sort(function (a, b) { return a.fuelConsuption - b.fuelConsuption; });
    };
    Autopark.prototype.searchBySpeed = function (cars) {
        return cars.sort(function (a, b) { return a.maxSpeed - b.maxSpeed; });
    };
    return Autopark;
}());
var Car = /** @class */ (function () {
    function Car(manufacturer, type, fuelConsumption, maxSpeed, price) {
        this._manufacturer = manufacturer;
        this._type = type;
        this._fuelConsuption = fuelConsumption;
        this._maxSpeed = maxSpeed;
        this._price = price;
    }
    Object.defineProperty(Car.prototype, "manufacturer", {
        get: function () {
            return this._manufacturer;
        },
        set: function (manufacturer) {
            this._manufacturer = manufacturer;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (type) {
            this._type = type;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "fuelConsuption", {
        get: function () {
            return this._fuelConsuption;
        },
        set: function (fuelConsuption) {
            this._fuelConsuption = fuelConsuption;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "maxSpeed", {
        get: function () {
            return this._maxSpeed;
        },
        set: function (maxSpeed) {
            this._maxSpeed = maxSpeed;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Car.prototype, "price", {
        get: function () {
            return this._price;
        },
        set: function (price) {
            this._price = price;
        },
        enumerable: false,
        configurable: true
    });
    Car.prototype.toString = function () {
        return "Car manufacturer is ".concat(this.manufacturer, ", type is ").concat(this.type, ", fuel consuption id ").concat(this.fuelConsuption, " and max speed is ").concat(this.maxSpeed);
    };
    return Car;
}());
var cars = [
    new Car('Volkswagen', 'sedan', 8.0, 180, 15000),
    new Car('BMW', 'suv', 12.0, 180, 30000),
    new Car('Mercedes', 'sedan', 9.0, 200, 30000),
];
var autopark = new Autopark();
var cost = autopark.totalCost(cars);
var fastestCars = autopark.searchBySpeed(cars);
var sortedByFuelConsuption = autopark.sortByFuelConsuption(cars);
console.log(cost, fastestCars, sortedByFuelConsuption);
