#include <time.h> 
#include <iostream> 
#include <stdlib.h> 
#include <ctime> 
#include "mpi.h"  
#include <stdio.h>
#define N 8  //кол-во строк

using namespace std;

void work(int n, int nl, int size, int rank)
{

	int ii, matr_rev;
	double** a = new double* [n];
	for (int i = 0; i < n; i++)
		a[i] = new double[nl];

	double** b = new double* [n];
	for (int i = 0; i < n; i++)
		b[i] = new double[nl];
	int i, j;
	//Определение данных, объявление массивов


	MPI_Status status;
	for (j = 0; j < nl; j++)
		for (i = 0; i < n; i++) {
			b[i][j] = 0.0;
			ii = j + rank * nl;
			a[i][j] = 100 * ii + i;
		}
	//Инициализация массива а[i][j],и перестановка столбцов в матрицу b[i][j]


	MPI_Type_vector(nl, n, -n, MPI_DOUBLE, &matr_rev);
	MPI_Type_commit(&matr_rev);
	MPI_Sendrecv(&a[0][nl - 1], 1, matr_rev, size - rank - 1, 1, &b[0][nl - 1], nl * n, MPI_DOUBLE, size - rank - 1, 1, MPI_COMM_WORLD, &status); // вывод данных на экран
	for (j = 0; j < nl; j++)
		for (i = 0; i < n; i++)
			printf("process %d : %d %d %f %f\n", rank, j + rank * nl, i, a[i][j], b[i][j]);

}
int main(int argc, char** argv)
{
	int rank, size, nl;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	nl = (N - 1) / size + 1; // кол-во столбцов
	work(N, nl, size, rank);   // функция перестановки столбцов матрицы
	MPI_Finalize();
}