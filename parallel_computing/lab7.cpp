#include <time.h> 
#include <iostream> 
#include <stdlib.h> 
#include <ctime> 
#include "mpi.h"  
#include <stdio.h> 

using namespace std;

int main(int argc, char** argv)
{
  int rank;
  MPI_File qw;
  char buf[10];
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_File_open(MPI_COMM_WORLD /*коммутатор*/, const_cast<char*>("lab7.txt"), MPI_MODE_RDONLY/*Тип доступа*/, MPI_INFO_NULL /*Информационный объект*/, &qw);
  MPI_File_set_view(qw, rank * 10 /*смещение*/, MPI_CHAR /*Элементарный тип данных*/, MPI_CHAR /*тип файла*/, const_cast<char*>("native") /*представление данных*/, MPI_INFO_NULL/*информационный объект*/);
  MPI_File_read_all(qw, buf/*начальный адрес*/, 10 /*количество элементов*/, MPI_CHAR/*тип данных*/, MPI_STATUS_IGNORE/*состояние*/);
  if(rank == 0)
  {
    cout << "process = " << rank << endl;
    cout << " buf =  ";
    for (int i = 0; i < 10; i++)
      cout << buf[i];
  }  
  
  cout << endl;
  MPI_File_close(&qw);
  MPI_Finalize();
}