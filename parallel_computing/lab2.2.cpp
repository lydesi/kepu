#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <math.h>
#include <stdio.h>

/* Идентификаторы сообщений */
#define tagFloatData 1
#define tagDoubleData 2

int main(int argc, char *argv[]) {
  int n = 10;
  int m = 1000;
  int k, i, rank, size, a;

  float floatData[10];
  double doubleData[20];
  
  MPI_Request request;
	MPI_Status status;

  MPI_Init(&argc, &argv);//MPI Initializez
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  /* и свой собственный номер: от 0 до (size-1) */
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  for (k = 0; k <= n; k++) {
    // printf("время Send= %13.4e время нагрузки= %13.4e \n", char(rank), char(rank));
    printf("%c ", char(rank));
    if (rank == 0) {
      double bb[1000];
      for (i = 0; i < 1000; i++) 
        bb[i] = i + 0;
      double t1 = MPI_Wtime();
      MPI_Send(bb, 1000, MPI_DOUBLE, 1, tagDoubleData, MPI_COMM_WORLD);
      
      double t2 = MPI_Wtime();
			for (int i = 1; i <= m; i++)
				a = sin(i) * cos(i) * pow((i + 0.), 0.75) * exp((i + 0.) / m);
      t2 = MPI_Wtime() - t2;
      printf("время Send= %13.4e время нагрузки= %13.4e \n", t1, t2);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 1) {
      double t3 = MPI_Wtime();
      double bb[1000];
      MPI_Status status;
      MPI_Recv(bb, 1000, MPI_DOUBLE, 0, tagDoubleData, MPI_COMM_WORLD, &status);
      t3 = MPI_Wtime() - t3;
      printf("время Recv= %13.4e\n", t3);
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
  }
  return 0;
}