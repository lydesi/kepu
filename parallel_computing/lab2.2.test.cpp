/* Простая передача-прием: MPI_Send, MPI_Recv Завершение по ошибке:
MPI_Abort */
#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <math.h>
#include <stdio.h>

/* Идентификаторы сообщений */
#define tagFloatData 1
#define tagDoubleData 2

int main(int argc, char** argv) {
  int rank; // Номер текущего процесса
	int numprocs; // Количество прцессов
	int rc;
	if (rc == MPI_Init(&argc, &argv))
	{
		MPI_Abort(MPI_COMM_WORLD, rc);
	}
  MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	int n = 10;
	int m = atoi(argv[1]); // 1000
	
	for (int k = 1; k <= n; k++) {
		if (rank == 0) {
			double bb[1000];
			for (int i = 0; i < 1000; i++)
				bb[i] = i + 0;
			double t1 = MPI_Wtime();
			MPI_Send(bb, 1000, MPI_DOUBLE, 1, 1, MPI_COMM_WORLD);
			t1 = MPI_Wtime() - t1;
			//// имитация вычислительной нагрузки
			double t2 = MPI_Wtime();
			for (int i = 1; i <= m; i++)
				double a = sin(i) * cos(i) * pow((i + 0.), 0.75) * exp((i + 0.) / m);
			t2 = MPI_Wtime() - t2;
			printf("time Send = % 13.4e \ntime load = % 13.4e \n", t1, t2);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		if (rank == 1) {
			double t3 = MPI_Wtime();
			double bb[1000];
			MPI_Status status;
			MPI_Recv(bb, 1000, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &status);
			t3 = MPI_Wtime() - t3;
			printf("time Recv = % 13.4e\n", t3);
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}

	return 0;
}