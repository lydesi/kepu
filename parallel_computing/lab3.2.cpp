/* В примере предполагается, что количество строк матрицы A и B делятся без остатка
* на количество компьютеров в системе.
* В данном случае задачу запускаем на 4 компьютерах. */
#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include "mpi.h"

/* Задаем в каждой ветви размеры полос матриц A и B. */
#define M 20
#define N 5
/* NUM_DIMS - размер декартовой топологии. "кольцо" - одномерный тор. */
#define DIMS 1
#define EL(x) (sizeof(x) / sizeof(x[0][0]))
/* Задаем полосы исходных матриц. В каждой ветви, в данном случае, они одинаковы */
static double A[N][M], B[M], C[N];
int main(int argc, char** argv)
{
	int rt, t1, t2, rank, size, * index, * edges, i, j, reord = 0;
	MPI_Comm comm_gr;
	/* Инициализация библиотеки MPI*/
	MPI_Init(&argc, &argv);
	/* Каждая ветвь узнает количество задач в стартовавшем приложении */
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	/* Выделяем память под массивы для описания вершин (index) и
	 ребер (edges) в топологии граф */
	index = (int*)malloc(size * sizeof(int));
	edges = (int*)malloc(((size - 1) + (size - 1) * 3) * sizeof(int));
	/* Заполняем массивы для описания вершин и ребер для топологии граф и задаем топологию "граф".
	*/
	index[0] = size - 1;
	for (i = 0; i < size - 1; i++)
		edges[i] = i + 1;
	
	int v = 0;
	for (i = 1; i < size; i++)
	{
		index[i] = (size - 1) + i * 3;
		edges[(size - 1) + v++] = 0;
		edges[(size - 1) + v++] = ((i - 2) + (size - 1)) % (size - 1) + 1;
		edges[(size - 1) + v++] = i % (size - 1) + 1;
	}
	MPI_Graph_create(MPI_COMM_WORLD, size, index, edges, reord,
		&comm_gr);
	/* Каждая ветвь определяет свой собственный номер: от 0 до (size-1) */
	MPI_Comm_rank(comm_gr, &rank);
	/* Каждая ветвь генерирует полосы исходных матриц A и B, полосы C обнуляет */
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < N; j++)
		{
			A[j][i] = 3.0;
			C[j] = 0.0;
		}
		B[i] = 2.0;
	}
	/* Засекаем начало умножения матриц */
	t1 = MPI_Wtime();
	/* Каждая ветвь производит умножение своих полос матрицы и вектора */
	for (j = 0; j < N; j++)
	{
		for (i = 0; i < M; i++)
			C[j] += A[j][i] * B[i];
	}
	/* Соединяем части вектора С и записываем их в вектор В во все компьютеры */
	MPI_Allgather(C, N, MPI_DOUBLE, B, N, MPI_DOUBLE, comm_gr);
	/* Каждая ветвь печатает время решения и нулевая ветвь печатает вектор В */
	t2 = MPI_Wtime();
	rt = t2 - t1;
	printf("rank = %d Time = %d\n", rank, rt);
	if (rank == 0)
	{
		for (i = 0; i < M; i++)
			printf("B = %6.2f\n", B[i]);
	}
	MPI_Finalize();
	return(0);
}
